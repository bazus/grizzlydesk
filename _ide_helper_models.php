<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\AddFields
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddFields newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddFields newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddFields query()
 */
	class AddFields extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $name
 * @property int $owner_id
 * @property string $slug
 * @property string $industry_slug
 * @property string $size
 * @property string $phone
 * @property string|null $websiteurl
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \App\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereIndustrySlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereWebsiteurl($value)
 */
	class Company extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_owner_id
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $companyname
 * @property string|null $aditional
 * @property int|null $company_as_primary
 * @property string $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereAditional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCompanyAsPrimary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCompanyOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCompanyname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Client whereUuid($value)
 */
	class Client extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\IndustryCategory
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Industry[] $industries
 * @property-read int|null $industries_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\IndustryCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\IndustryCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\IndustryCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\IndustryCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\IndustryCategory whereName($value)
 */
	class IndustryCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Industry
 *
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property int $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Industry whereSlug($value)
 */
	class Industry extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Leads
 *
 * @property int $id
 * @property string $uuid
 * @property string|null $title
 * @property int $company_id
 * @property string $client_uuid
 * @property int $manual_created
 * @property string|null $aditional
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereAditional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereClientUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereManualCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Leads whereUuid($value)
 */
	class Leads extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Company|null $company
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

