
$(document).ready(function () {

    let tableRequests = $('.table-requests').DataTable({
        sDom: 'lrtip',
        lengthChange: false,
        pageLength: 5,
        "language": {
            "paginate": {
                "previous": '<i class="mdi mdi-chevron-left"></i>',
                "next": '<i class="mdi mdi-chevron-right"></i>',
            }
        },
        "drawCallback": function () {
            $(".table-requests-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
        }

    });

    let tableQuotes = $('.table-quotes').DataTable({
        sDom: 'lrtip',
        lengthChange: false,
        pageLength: 5,
        "language": {
            "paginate": {
                "previous": '<i class="mdi mdi-chevron-left"></i>',
                "next": '<i class="mdi mdi-chevron-right"></i>',
            }
        },
        "drawCallback": function () {
            $(".table-qoutes-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
        }

    });
    let tableJobs = $('.table-jobs').DataTable({
        sDom: 'lrtip',
        lengthChange: false,
        pageLength: 5,
        "language": {
            "paginate": {
                "previous": '<i class="mdi mdi-chevron-left"></i>',
                "next": '<i class="mdi mdi-chevron-right"></i>',
            }
        },
        "drawCallback": function () {
            $(".table-jobs-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
        }

    });
    let tableInvoices = $('.table-invoices').DataTable({
        sDom: 'lrtip',
        lengthChange: false,
        pageLength: 5,
        "language": {
            "paginate": {
                "previous": '<i class="mdi mdi-chevron-left"></i>',
                "next": '<i class="mdi mdi-chevron-right"></i>',
            }
        },
        "drawCallback": function () {
            $(".table-invoices-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
        }

    });




    $(".table-requests-paginaion-container").append($("#DataTables_Table_0_paginate"));
    $(".table-requests-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    $(".table-qoutes-paginaion-container").append($("#DataTables_Table_1_paginate"));
    $(".table-qoutes-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    $(".table-jobs-paginaion-container").append($("#DataTables_Table_2_paginate"));
    $(".table-jobs-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    $(".table-invoices-paginaion-container").append($("#DataTables_Table_3_paginate"));
    $(".table-invoices-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');

})
