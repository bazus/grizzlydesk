function preloadImages(array) {
    if (!preloadImages.list) {
        preloadImages.list = [];
    }
    let list = preloadImages.list;
    for (let i = 0; i < array.length; i++) {
        let img = new Image();
        img.onload = function() {
            let index = list.indexOf(this);
            if (index !== -1) {
                list.splice(index, 1);
            }
        }
        list.push(img);
        img.src = array[i];
    }
}
function formatState(data, container) {

    if(!data.id) return data.text
    return $(
        '<span><img src="' + data.flag + '" class="img-flag" /> ' + data.text + '</span>'
    )
}
function getCountries() {
   let images = []
    $.getJSON('/resources/counties.json',function (data) {
        let countries = data.countries.map(function (e) {
            return {
                id: e.code,
                text: e.name,
                flag: e.flag
            }
        })
        $('.country-select').select2({
            data: countries,
            templateResult: formatState
        })
    });


}

$(document).ready(function () {
    getCountries();

});
