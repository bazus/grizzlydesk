@extends('layouts.auth-page')
@push('content')

    <div class="col-md-10 col-lg-8 col-xl-7">
        <div class="card overflow-hidden">
            <div class="bg-soft-primary">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-4">
                            <h5 class="text-primary">Set up your company</h5>
                            <p>Give us some additional data about your company.</p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ asset('assets/images/company/business.svg') }}" alt="" class="img-fluid">
                    </div>


                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    <div class="col-4">
                        <a href="index.html">
                            <div class="avatar-md profile-user-wid mb-4">
                                <span class="avatar-title rounded-circle bg-light">
                                    <img src="{{ asset('assets/images/logo.png') }}" alt="" class="rounded-circle" height="55">
                                </span>
                            </div>
                        </a>
                    </div>

                    <div class="col-8 text-right">

                            <button type="button" class="btn header-item text-right" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="d-inline-block ml-1">{{ Auth::user()->email }}</span>
                                <div class="avatar-xs d-inline-block ml-1">
                                    <span class="avatar-title rounded-circle">
                                        {{ Auth::user()->firstname[0] }}
                                    </span>
                                </div>
                                <i class="mdi mdi-chevron-down d-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" style="">

                                <a class="dropdown-item text-danger" href="{{ route('logout') }}"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout</a>
                            </div>

                    </div>
                </div>


                <x-auth.steps :step="2"/>

                <div class="p-2">
                <form class="form-horizontal" method="POST" action="{{ route('panel.company.store') }}">

                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="companyname">Company name</label>
                                <input value="{{ old('name') }}" name="name" type="text" autocomplete="companyname"
                                class="form-control @error('name') is-invalid @enderror" id="companyname" placeholder="Enter company name">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="companysize">Company size</label>
                                <div class="col-lg-12 pl-0 pr-0">
                                    <select name="size" class="custom-select  @error('size') is-invalid @enderror" id="companysize">
                                        <option value="" selected="">Workers count</option>
                                        <option value="1">Just me</option>
                                        <option value="2">2-3 people</option>
                                        <option value="3">4-10 people</option>
                                        <option value="4">10+ people</option>
                                    </select>
                                    @error('size')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="companyphone">Phone number</label>
                                <input value="{{ old('phone') }}" name="phone" type="tel" autocomplete="companyphone"
                                class="form-control @error('phone') is-invalid @enderror" id="companyphone" placeholder="Enter phone number">
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label class="control-label" for="companyindustry">Industry</label>
                                <select name="industry_slug" class="form-control @error('industry_slug') is-invalid @enderror custom-select" id="companyindustry">
                                    <option value="">Select</option>
                                    @foreach ($industry_categories as $industry_category)
                                        <optgroup label="{{ $industry_category->name }}">
                                            @foreach ($industry_category->industries as $industry)
                                                <option value="{{$industry->slug}}">{{$industry->name}}</option>
                                            @endforeach


                                        </optgroup>
                                    @endforeach
                                </select>
                                @error('industry_slug')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">

                            <div class="custom-control custom-checkbox custom-checkbox-outline custom-checkbox-primary mb-3">
                                <input type="checkbox" class="custom-control-input" id="customCheck-outlinecolor1">
                                <label class="custom-control-label" for="customCheck-outlinecolor1">We already have a website</label>
                            </div>
                            <div class="collapse form-group" id="collapseExample" style="">

                                <input value="{{ old('websiteurl') }}" name="websiteurl" type="text" autocomplete="websiteurl"
                                class="form-control @error('websiteurl') is-invalid @enderror" id="websiteurl" placeholder="Url of your website">
                                @error('websiteurl')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>



                    <div class="mt-4">
                      <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Continue</button>
                    </div>
                </form>
                </div>


            </div>
        </div>


    </div>

@endpush
