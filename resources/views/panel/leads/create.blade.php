@extends('layouts.panel-page')

@push('links')
    <link href="{{ asset('assets/libs/bootstrap-editable/bootstrap-editable.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@push('page.content')
    <div class="container-fluid">

        <x-page-title>
            Create new request
        </x-page-title>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('panel.leads.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group title mb-4">
                                        <h3>
                                            Request for <span type="button" data-toggle="modal" data-target=".choose-user" class="pointer editable editable-pre-wrapped editable-click editable-unsaved user-name">not selected</span>
                                            <input type="hidden" name="client_uuid">
                                        </h3>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <x-apps.input name="title" type="text">
                                        Title
                                    </x-apps.input>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="formrow-aditional-input">Aditional info</label>
                                        <textarea rows="6" name="aditional" type="text" class="form-control @error('aditional') is-invalid @enderror" id="formrow-aditional-input">{{old('aditional')}}</textarea>
                                        @error('aditional')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>





                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->


    <x-modals.select-user/>

    <x-modals.create-user/>
@endpush

@push('scripts')
@endpush
