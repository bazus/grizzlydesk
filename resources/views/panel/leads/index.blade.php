@extends('layouts.panel-page')

@push('page.content')
    <div class="container-fluid">

        <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Requests List</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Contacts</li>
                        <li class="breadcrumb-item">User List</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
     <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <div class="search-box mr-2 mb-2 d-inline-block">
                                <div class="position-relative">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="bx bx-search-alt search-icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <a href="{{ route('panel.leads.create') }}">
                                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 mr-2">
                                        <i class="mdi mdi-plus mr-1"></i> Create new request
                                    </button>
                                </a>
                            </div>
                        </div><!-- end col-->
                    </div>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap table-hover">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Client</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Contact</th>
                                    <th scope="col">Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leads as $lead)

                                        <tr data-href="{{ route('panel.leads.show', $lead->uuid) }}">

                                            <td>
                                                <a href="{{ route('panel.clients.show', $lead->client->uuid) }}">
                                                    <div class="d-flex justify-content-start align-items-center">


                                                        <div class="avatar-xs">
                                                            <span class="avatar-title rounded-circle">
                                                                {{$lead->client->firstname[0]}}
                                                            </span>
                                                        </div>
                                                        <div class="d-flex ml-2">
                                                            <span>
                                                                {{ $lead->client->name() }}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>

                                            </td>

                                            <td>
                                                {{ $lead->title }}
                                            </td>
                                            <td>{{$lead->client->email}}</td>

                                            <td>
                                                {{ $lead->created_at }}
                                            </td>

                                        </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{ $leads->links('components.apps.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endpush
