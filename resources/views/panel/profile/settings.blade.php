@extends('layouts.panel-page')

@push('links')
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('page.content')
    <div class="container-fluid">

        <x-page-title>
            Settings
        </x-page-title>
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <h5 class="font-size-14 my-3 font-weight-bold">Business management</h5>
                                    <a class="nav-link mb-2 active" id="v-pills-company-settings-tab" data-toggle="pill" href="#v-pills-company-settings" role="tab" aria-controls="#v-pills-company-settings" aria-selected="true">Company Settings</a>
                                    <a class="nav-link mb-2" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Branding</a>
                                    <a class="nav-link mb-2" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Products & Services</a>
                                    <a class="nav-link mb-2" id="v-pills-custom-fields-tab" data-toggle="pill" href="#v-pills-custom-fields" role="tab" aria-controls="v-pills-custom-fields" aria-selected="false">Custom Fields</a>
                                    <h5 class="font-size-14 my-3 font-weight-bold">Team organization</h5>
                                    <a class="nav-link mb-2" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Manage Team</a>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Work Settings</a>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Calendar</a>
                                    <h5 class="font-size-14 my-3 font-weight-bold">Client communication</h5>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Client Hub</a>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Templates</a>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Notifications</a>
                                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Requests</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="tab-content " id="v-pills-tabContent">
                    <x-settigns-page.company-settings id="v-pills-company-settings"/>
                    <x-settigns-page.custom-fields/>
                </div>
            </div>


        </div>

    </div> <!-- container-fluid -->
@endpush

@push('scripts')
    <script src="{{ asset('assets/libs/select2/select2.min.js')}}"></script>

    <script src="{{ asset('assets/js/apps/settings.js')}}"></script>
@endpush
