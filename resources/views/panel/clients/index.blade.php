@extends('layouts.panel-page')

@push('page.content')
    <div class="container-fluid">

        <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">User List</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Contacts</li>
                        <li class="breadcrumb-item">User List</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
     <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <div class="search-box mr-2 mb-2 d-inline-block">
                                <div class="position-relative">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="bx bx-search-alt search-icon"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <a href="{{ route('panel.clients.create') }}">
                                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 mr-2">
                                        <i class="mdi mdi-plus mr-1"></i> Create new client
                                    </button>
                                </a>
                            </div>
                        </div><!-- end col-->
                    </div>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap table-hover">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" style="width: 70px;">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Tags</th>
                                    <th scope="col">Projects</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clients as $client)

                                    <tr data-href="{{route('panel.clients.show', $client->uuid)}}">
                                        <td>
                                            <div class="avatar-xs">
                                                <span class="avatar-title rounded-circle">
                                                    {{$client->firstname[0]}}
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <h5 class="font-size-14 mb-1"><a href="{{ route('panel.clients.show', $client->uuid)}}" class="text-dark">{{$client->name()}}</a></h5>
                                            @if($client->companyname)
                                                <p class="text-muted mb-0">{{$client->companyname}}</p>
                                            @endif
                                        </td>
                                        <td>{{$client->email}}</td>
                                        <td>
                                            <div>
                                                <a href="#" class="badge badge-soft-primary font-size-11 m-1"></a>
                                                <a href="#" class="badge badge-soft-primary font-size-11 m-1"></a>
                                            </div>
                                        </td>
                                        <td>
                                            125
                                        </td>
                                        <td>
                                            <ul class="list-inline font-size-20 contact-links mb-0">
                                                <li class="list-inline-item px-2">
                                                    <a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Message"><i class="bx bx-message-square-dots"></i></a>
                                                </li>
                                                <li class="list-inline-item px-2">
                                                    <a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile"><i class="bx bx-user-circle"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{ $clients->links('components.apps.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endpush
