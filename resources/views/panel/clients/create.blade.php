@extends('layouts.panel-page')

@push('links')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
        <!-- dropzone css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}">
@endpush

@push('page.content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Create New Client</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Clients</a></li>
                            <li class="breadcrumb-item active">Create New </li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Client info</h4>
                        <form method="POST" action="{{ route('panel.clients.store') }}">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <x-apps.input name="firstname" type="text">
                                        First Name
                                    </x-apps.input>

                                </div>
                                <div class="col-md-6">
                                    <x-apps.input name="lastname" type="text">
                                        Last Name
                                    </x-apps.input>
                                </div>
                                <div class="col-md-12">

                                    <x-apps.input name="companyname" type="text">
                                        Company name
                                    </x-apps.input>
                                    <div class="custom-control custom-checkbox custom-checkbox-outline custom-checkbox-primary mb-3">
                                        <input name="company_as_primary" type="checkbox" class="custom-control-input" id="company_as_primary">
                                        <label class="custom-control-label" for="company_as_primary">Company name as primary name</label>
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <x-apps.input name="email" type="email">
                                        Email
                                    </x-apps.input>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">

                                    <x-apps.input name="phone" type="text">
                                        Phone
                                    </x-apps.input>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="formrow-aditional-input">Aditional info</label>
                                        <textarea rows="6" name="aditional" type="text" class="form-control @error('aditional') is-invalid @enderror" id="formrow-aditional-input">{{old('aditional')}}</textarea>
                                        @error('aditional')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
@endpush

@push('scripts')
@endpush
