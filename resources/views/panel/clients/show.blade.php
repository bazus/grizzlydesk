@extends('layouts.panel-page')

@push('links')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/datatables/datatables.min.css')}}">
@endpush

@push('page.content')

    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Profile</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Contacts</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="media">
                                    <div class="mr-3">
                                        <div class="avatar-md rounded-circle img-thumbnail">
                                                <span class="avatar-title rounded-circle">
                                                    {{ $client->firstname[0].$client->lastname[0] }}
                                                </span>
                                        </div>
                                    </div>
                                    <div class="media-body align-self-center">
                                        <div class="text-muted">

                                            <h5 class="mb-1">{{ $client->name() }}</h5>
                                            <p class="mb-0">{{ $client->email }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 align-self-center">
                                <div class="text-lg-center mt-4 mt-lg-0">
                                    <div class="row">
                                        <div class="col-4">
                                            <div>
                                                <p class="text-muted text-truncate mb-2">Requests</p>
                                                <h5 class="mb-0">48</h5>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div>
                                                <p class="text-muted text-truncate mb-2">Quotes</p>
                                                <h5 class="mb-0">40</h5>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div>
                                                <p class="text-muted text-truncate mb-2">Jobs</p>
                                                <h5 class="mb-0">18</h5>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 d-none d-lg-block">
                                <div class="clearfix  mt-4 mt-lg-0">
                                    <div class="dropdown float-right">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bxs-cog align-middle mr-1"></i> Edit Client
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-lg-4">

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Personal Information</h4>

{{--                        <p class="text-muted mb-4">Hi I'm Cynthia Price,has been the industry's standard dummy text To an English person, it will seem like simplified English, as a skeptical Cambridge.</p>--}}
                        <div class="table-responsive">
                            <table class="table table-nowrap mb-0">
                                <tbody class="text-wrap">
                                    <tr>
                                        <th scope="row">Full Name :</th>
                                        <td><p class="text-wrap">{{ $client->name() }}</p></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone :</th>
                                        <td><p class="text-wrap">{{ $client->phone }}</p></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Company name :</th>
                                        <td><p class="text-wrap">{{ $client->companyname }}</p></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">E-mail :</th>
                                        <td><p class="text-wrap">{{ $client->email }}</p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card -->


                <div class="card border border-primary">
                    <div class="card-header bg-transparent border-primary">
                        <h5 class="my-0 text-primary"><i class="mdi mdi-cash-usd-outline mr-3"></i>Billing history</h5>
                    </div>
                    <div class="card-body p-0 pt-2">
                        <div data-simplebar style="max-height: 230px;">

                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title bg-success rounded-circle font-size-16">
                                            <i class="bx bx-badge-check"></i>
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">No billing history yet</h6>
                                        <div class="font-size-12 text-muted">
                                            <p class="mb-1">This client hasn't been billed yet</p>
{{--                                            <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>--}}
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>

                        <div class="bg-primary avatar-title p-2">
                            <p class="m-0">Current balance $0.00</p>
                        </div>
                        <p class="my-2 text-truncate text-center">A draft invoice worth $350.00 needs to be sent</p>
                    </div>
                </div>
            </div>

            <div class="col-md-7 col-lg-8">

                <div class="card">
                    <div class="card-header bg-transparent border-primary">
                        <h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow mr-3"></i>Overview</h5>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-pills nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">Requests</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                    <span class="d-none d-sm-block">Quotes</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                    <span class="d-none d-sm-block">Jobs</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#settings1" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                    <span class="d-none d-sm-block">Invoices</span>
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content p-3 text-muted">
                            <div class="tab-pane active" id="home1" role="tabpanel">
                                <table class="table table-hover table-requests">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($i = 0; $i < rand(20, 40); $i++)
                                            <tr>
                                                <td @if($i==0)class="border-top-0"@endif>{{ $faker->sentence }}</td>
                                                <td class="text-center @if($i==0) border-top-0 @endif">{{ $faker->dateTimeThisYear->format('Y-m-d H:i:s') }}</td>
                                            </tr>
                                        @endfor
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12 table-requests-paginaion-container">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile1" role="tabpanel">
                                <table class="table table-hover table-quotes">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @for($i = 0; $i < rand(10, 30); $i++)
                                        <tr>
                                            <td @if($i==0)class="border-top-0"@endif>{{ $faker->sentence }}</td>
                                            <td class="text-center @if($i==0) border-top-0 @endif">{{ $faker->dateTimeThisYear->format('Y-m-d H:i:s') }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12 table-qoutes-paginaion-container">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="messages1" role="tabpanel">
                                <table class="table table-hover table-jobs">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @for($i = 0; $i < rand(1, 45); $i++)
                                        <tr>
                                            <td @if($i==0)class="border-top-0"@endif>{{ $faker->sentence }}</td>
                                            <td class="text-center @if($i==0) border-top-0 @endif">{{ $faker->dateTimeThisYear->format('Y-m-d H:i:s') }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12 table-jobs-paginaion-container">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="settings1" role="tabpanel">
                                <table class="table table-hover table-invoices">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @for($i = 0; $i < rand(12, 25); $i++)
                                        <tr>
                                            <td @if($i==0)class="border-top-0"@endif>{{ $faker->sentence }}</td>
                                            <td class="text-center @if($i==0) border-top-0 @endif">{{ $faker->dateTimeThisYear->format('Y-m-d H:i:s') }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12 table-invoices-paginaion-container">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-transparent border-primary">
                        <h5 class="my-0 text-primary"><i class="mdi mdi-calendar-today mr-3"></i>SCHEDULE</h5>
                    </div>
                    <div class="card-body">

                        <h4 class="card-title mb-4">My Projects</h4>
                        <div class="table-responsive">
                            <table class="table table-nowrap table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Projects</th>
                                        <th scope="col">Start Date</th>
                                        <th scope="col">Deadline</th>
                                        <th scope="col">Budget</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Skote admin UI</td>
                                        <td>2 Sep, 2019</td>
                                        <td>20 Oct, 2019</td>
                                        <td>$506</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Skote admin Logo</td>
                                        <td>1 Sep, 2019</td>
                                        <td>2 Sep, 2019</td>
                                        <td>$94</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Redesign - Landing page</td>
                                        <td>21 Sep, 2019</td>
                                        <td>29 Sep, 2019</td>
                                        <td>$156</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>App Landing UI</td>
                                        <td>29 Sep, 2019</td>
                                        <td>04 Oct, 2019</td>
                                        <td>$122</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td>Blog Template</td>
                                        <td>05 Oct, 2019</td>
                                        <td>16 Oct, 2019</td>
                                        <td>$164</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td>Redesign - Multipurpose Landing</td>
                                        <td>17 Oct, 2019</td>
                                        <td>05 Nov, 2019</td>
                                        <td>$192</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">7</th>
                                        <td>Logo Branding</td>
                                        <td>04 Nov, 2019</td>
                                        <td>05 Nov, 2019</td>
                                        <td>$94</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->

@endpush

@push('scripts')
    <script src="{{ asset('assets/libs/datatables/datatables.min.js')}}"></script>

    <script src="{{ asset('assets/js/apps/clients/profile.js') }}"></script>
@endpush
