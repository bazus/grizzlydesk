@extends('layouts.auth-page')
@push('content')

    <div class="col-md-8 col-lg-6 col-xl-5">
        <div class="card overflow-hidden">
            <div class="bg-soft-primary">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-4">
                            <h5 class="text-primary">Free Register</h5>
                            <p>Get your free Skote account now.</p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ asset('assets/images/profile-img.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div>
                    <a href="index.html">
                        <div class="avatar-md profile-user-wid mb-4">
                            <span class="avatar-title rounded-circle bg-light">
                                <img src="{{ asset('assets/images/logo.png') }}" alt="" class="rounded-circle" height="55">
                            </span>
                        </div>
                    </a>
                </div>


                <x-auth.steps :step="1"/>

                <div class="p-2">
                <form class="form-horizontal" method="POST" action="{{ route('auth.request.register') }}">

                    @csrf

                    <div class="form-group">
                        <label for="useremail">Email</label>
                        <input value="{{ old('email') }}" name="email" type="email" autocomplete="email"
                        class="form-control @error('email') is-invalid @enderror" id="useremail" placeholder="Enter email">
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userFirstName">First name</label>
                                <input value="{{ old('firstname') }}" name="firstname" type="text" autocomplete="firstname"
                                class="form-control @error('firstname') is-invalid @enderror" id="userFirstName" placeholder="Enter your first name">
                                @error('firstname')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userLastName">Last name</label>
                                <input value="{{ old('lastname') }}" name="lastname" type="text" autocomplete="lastname"
                                class="form-control @error('lastname') is-invalid @enderror" id="userLastName" placeholder="Enter your last name">
                                @error('lastname')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="userpassword">Password</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        id="userpassword" placeholder="Enter password" autocomplete="newUserPassword">
                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mt-4">
                      <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Register</button>
                    </div>

                    <div class="mt-4 text-center">
                        <h5 class="font-size-14 mb-3">Sign up using</h5>

                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="javascript::void()" class="social-list-item bg-primary text-white border-primary">
                                    <i class="mdi mdi-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript::void()" class="social-list-item bg-info text-white border-info">
                                    <i class="mdi mdi-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript::void()" class="social-list-item bg-danger text-white border-danger">
                                    <i class="mdi mdi-google"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="mt-4 text-center">
                        <p class="mb-0">By registering you agree to the Skote <a href="#" class="text-primary">Terms of Use</a></p>
                    </div>
                </form>
                </div>


            </div>
        </div>
        <div class="mt-5 text-center">

            <div>
                <p>Already have an account ? <a href="{{ route('auth.page.login') }}" class="font-weight-medium text-primary"> Login</a> </p>
                <p>© 2020 Skote. Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</p>
            </div>
        </div>

    </div>
@endpush
