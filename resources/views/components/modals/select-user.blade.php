@push('links')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/datatables/datatables.min.css')}}">
@endpush
<div class="modal fade choose-user" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content pl-3 pt-3 pr-3 pb-0">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Select or create client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-8">

                        <x-apps.input name="search" type="text">
                            <x-slot name="placeholder">
                                Search
                            </x-slot>
                        </x-apps.input>
                    </div>

                    <div class="col-4">
                        <button type="button"
                                class="btn btn-success waves-effect waves-light w-100"
                                data-toggle="modal"
                                data-target=".create-user"
                                data-dismiss="modal">Create new</button>


                    </div>

                </div>
                <div class="table-responsive">

                       <table id="datatable-buttons" class="table table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                           <thead class="thead-light">
                               <tr>
                                   <th class="no-sort">@</th>
                                   <th>Client</th>
                                   <th>Email</th>
                               </tr>
                           </thead>


                           <tbody>
                               @foreach($clients as $client)
                                   <tr data-user="{{$client->uuid}}" style="cursor: pointer">
                                       <td>
                                           <div class="avatar-xs">
                                                <span class="avatar-title rounded-circle">
                                                    {{ $client->firstname[0] }}
                                                </span>
                                           </div>
                                       </td>
                                       <td>
                                           <h5 class="font-size-14 mb-1"><a href="#" class="text-dark">{{ $client->name() }}</a></h5>
                                            @isset($client->company)
                                                <p class="text-muted mb-0">{{ $client->companyname }}</p>
                                            @endisset
                                       </td>
                                       <td>
                                           {{$client->email}}
                                       </td>
                                   </tr>
                               @endforeach
                           </tbody>
                       </table>
                </div>
                <div class="row">
                    <div class="col-lg-12 table-paginaion-container">
                    </div>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@push('scripts')
    <!-- Plugins js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js')}}"></script>

    <script src="{{ asset('assets/js/apps/select-user.js')}}"></script>
@endpush
