<div class="modal fade create-user overflow-auto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0 pl-3">Create new client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <form method="POST" action="http://grizzlydesk.bazus.net/panel/clients">
                    <input type="hidden" name="_token" value="IESlKnkDSwNMKQcUjTXRCVqZVxhsD0OXJy1FbVGd">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form-input-firstname">First Name</label>

                                <input value="" name="firstname" type="text" autocomplete="firstname" class="form-control " id="form-input-firstname">
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form-input-lastname">Last Name</label>

                                <input value="" name="lastname" type="text" autocomplete="lastname" class="form-control " id="form-input-lastname">
                            </div>

                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="form-input-companyname">Company name</label>

                                <input value="" name="companyname" type="text" autocomplete="companyname" class="form-control " id="form-input-companyname">
                            </div>

                            <div class="custom-control custom-checkbox custom-checkbox-outline custom-checkbox-primary mb-3">
                                <input name="company_as_primary" type="checkbox" class="custom-control-input" id="company_as_primary">
                                <label class="custom-control-label" for="company_as_primary">Company name as primary name</label>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="form-input-email">Email</label>

                                <input value="" name="email" type="email" autocomplete="email" class="form-control " id="form-input-email">
                            </div>


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group">
                                <label for="form-input-phone">Phone</label>

                                <input value="" name="phone" type="text" autocomplete="phone" class="form-control " id="form-input-phone">
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="formrow-aditional-input">Aditional info</label>
                                <textarea rows="6" name="aditional" type="text" class="form-control " id="formrow-aditional-input"></textarea>
                            </div>
                        </div>
                    </div>


                    <div>
                        <button type="submit" class="btn btn-primary w-md">Submit</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
