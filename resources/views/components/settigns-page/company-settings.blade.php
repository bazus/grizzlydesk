<div class="tab-pane fade show active" id="{{ $id }}" role="tabpanel" aria-labelledby="v-pills-company-settings-tab">
    <div class="card">
        <div class="card-body">
            <div class="card-title mb-4">
                <h4>Company Settings</h4>
            </div>
            <form action="">
                <fieldset>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="test-input">First Name</label>
                                <input id="test-input" class="form-control custom-input" type="text" placeholder="">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <x-apps.input name="name" type="text">
                                Company Name
                            </x-apps.input>
                        </div>

                        <div class="col-md-12">
                            <x-apps.input name="phone" type="text">
                                Phone number
                            </x-apps.input>
                        </div>
                        <div class="col-md-12">
                            <x-apps.input name="websiteurl" type="text">
                                Website URL
                            </x-apps.input>
                        </div>
                        <div class="col-md-12">
                            <x-apps.input name="email" type="email">
                                Email address
                            </x-apps.input>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="mt-4">

                    <div class="row">
                        <div class="col-md-12">
                            <x-apps.input name="Street 1" type="text">
                                Street 1
                            </x-apps.input>
                        </div>
                        <div class="col-md-12">
                            <x-apps.input name="Street 2" type="text">
                                Street 2
                            </x-apps.input>
                        </div>
                        <div class="col-md-6">
                            <x-apps.input name="city" type="text">
                                City
                            </x-apps.input>
                        </div>
                        <div class="col-md-6">
                            <x-apps.input name="state" type="text">
                                State
                            </x-apps.input>
                        </div>
                        <div class="col-md-6">
                            <x-apps.input name="zip" type="text">
                                Zip code
                            </x-apps.input>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group mb-0">
                                <label class="control-label">Country</label>
                                <select name="country" class="form-control country-select "></select>

                            </div>
                        </div>

                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>
