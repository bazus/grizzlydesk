<nav class="navbar navbar-expand-lg navigation fixed-top {{$type}}">
    <div class="container">
        <a class="navbar-logo" href="{{ route('landing') }}">
            <img src="assets/images/logo-dark.png" alt="" height="45" class="logo logo-dark">
            <img src="assets/images/logo-full.png" alt="" height="45" class="logo logo-light">
        </a>

        <button type="button" class="btn btn-sm px-3 font-size-16 d-lg-none header-item waves-effect waves-light" data-toggle="collapse" data-target="#topnav-menu-content">
            <i class="fa fa-fw fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="topnav-menu-content">
            <ul class="navbar-nav ml-auto" id="topnav-menu" >
                <li class="nav-item">
                    <a class="nav-link" href="{{route('landing')}}#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('landing')}}#features">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('landing')}}#industries">Industries</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('landing')}}#news">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('landing')}}#faqs">FAQs</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('pricing') }}">Pricing</a>
                </li>

            </ul>

            <div class="ml-lg-2">
                @guest
                    <a href="{{ route('auth.page.login') }}" class="btn btn-outline-success w-xs waves-effect">Sign in</a>
                    <a href="{{ route('auth.page.register') }}" class="btn btn-success w-xs waves-effect">Sign up</a>
                @endguest
                @auth
                    <a href="{{ route('auth.page.login') }}" class="btn btn-outline-success w-xs waves-effect">Logout</a>
                    <a href="{{ route('panel.dashboard') }}" class="btn btn-success w-xs waves-effect">{{ Auth::user()->firstname }}</a>
                @endauth
            </div>
        </div>
    </div>
</nav>
