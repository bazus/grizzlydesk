<div class="form-group">
    @if(isset($slot) && Str::length($slot) > 0 )
        <label for="{{ isset($id)? $id : 'form-input-'.$name }}">{{ $slot }}</label>
    @endif

    <input
        value="{{ old($name) }}"
        name="{{ $name }}"
        type="{{ $type }}"
        autocomplete="{{ $name }}"
        class="form-control @error($name) is-invalid @enderror"
        id="{{ isset($id)? $id : 'form-input-'.$name }}"
        @if(isset($placeholder))
        placeholder="{{ $placeholder }}"
        @endif

        >
    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
