<!-- Footer start -->
<footer class="landing-footer">
    <div class="container">

        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="mb-4 mb-lg-0">
                    <h4 class="mb-4 footer-list-title">INDUSTRIES WE SERVE</h4>
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-unstyled footer-list-menu">
                                <li><a href="#">Appliance Repair</a></li>
                                <li><a href="#">Commercial Cleaning</a></li>
                                <li><a href="#">HVAC</a></li>
                                <li><a href="#">Lawn Care</a></li>
                                <li><a href="#">Pest Control</a></li>
                                <li><a href="#">Pool Service</a></li>
                                <li><a href="#">Residential Cleaning</a></li>
                                <li><a href="#">Tree Care</a></li>

                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list-unstyled footer-list-menu">
                                <li><a href="#">Carpet Cleaning</a></li>
                                <li><a href="#">Electrical Contracting</a></li>
                                <li><a href="#">Landscaping</a></li>
                                <li><a href="#">Painting</a></li>
                                <li><a href="#">Plumbing</a></li>
                                <li><a href="#">Pressure Washing</a></li>
                                <li><a href="#">Snow Removal</a></li>
                                <li><a href="#">More...</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset-lg-1 col-lg-2 col-sm-6">
                <div class="mb-4 mb-lg-0">
                    <h4 class="mb-4 footer-list-title">Features</h4>
                    <ul class="list-unstyled footer-list-menu">
                        <li><a href="#">Scheduling</a></li>
                        <li><a href="#">Invoicing</a></li>
                        <li><a href="#">Client Manager (CRM)</a></li>
                        <li><a href="#">Drag and Drop Calendar</a></li>
                        <li><a href="#">Credit Card Processing</a></li>
                        <li><a href="#">Jobs</a></li>
                        <li><a href="#">Business Financing</a></li>
                        <li><a href="#">More...</a></li>


                    </ul>
                </div>
            </div>
            <div class="offset-lg-1 col-lg-2 col-sm-6">
                <div class="mb-4 mb-lg-0">
                    <h4 class="mb-4 footer-list-title">RESOURCES</h4>
                    <ul class="list-unstyled footer-list-menu">
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Integrations</a></li>
                        <li><a href="#">Tour</a></li>
                        <li><a href="#">Academy</a></li>
                        <li><a href="#">Community</a></li>
                        <li><a href="#">Built by You Podcast</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Support</a></li>


                    </ul>
                </div>
            </div>

            <div class="offset-lg-1 col-lg-2 col-sm-6">
                <div class="mb-4 mb-lg-0">
                    <h4 class="mb-4 footer-list-title">COMPANY</h4>
                    <ul class="list-unstyled footer-list-menu">
                        <li><a href="#">Our Story</a></li>
                        <li><a href="#">Our Team</a></li>
                        <li><a href="#">Press & Media</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Home Service Trends</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">1-888-721-1115</a></li>
                        <li><a href="#">hello@grizzlydesk.com</a></li>


                    </ul>
                </div>
            </div>



        </div>
        <!-- end row -->

        <hr class="footer-border my-5">

        <div class="row">
            <div class="col-lg-6">
                <div class="mb-4">
                    <img src="assets/images/logo-full.png" alt="" height="35">
                </div>

                <p class="mb-2">2020 © Grizzlydesk.com</p>
                <p>All rights reserved</p>
            </div>

        </div>
    </div>
    <!-- end container -->
</footer>
<!-- Footer end -->
