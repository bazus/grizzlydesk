<div class="twitter-bs-wizard">
    <ul class="twitter-bs-wizard-nav nav nav-pills nav-justified">
        <li class="nav-item">
            <a href="#" class="nav-link @if($step == 1) active @else disabled @endif" data-toggle="tab">
                <span class="step-number mr-2">01</span>
                User

            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link @if($step == 2) active @else disabled @endif" data-toggle="tab">
                <span class="step-number mr-2">02</span>
                <span>Company</span>
            </a>
        </li>
    </ul>
</div>
