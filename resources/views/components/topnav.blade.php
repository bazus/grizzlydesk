<div class="topnav">
        <div class="container-fluid">
            <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                <div class="collapse navbar-collapse" id="topnav-menu-content">
                    <ul class="navbar-nav">

                        <li class="nav-item">
                            <a class="nav-link arrow-none" href="{{ route('panel.dashboard') }}" role="link"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-home-circle mr-2"></i>Dashboard
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-calendar" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-calendar-event mr-2"></i>Calendar <div class="arrow-down"></div>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="topnav-calendar">

                                <a href="#" class="dropdown-item">Month</a>
                                <a href="#" class="dropdown-item">Week</a>
                                <a href="#" class="dropdown-item">Grid</a>
                                <a href="#" class="dropdown-item">Map</a>
                                <a href="#" class="dropdown-item">List</a>

                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-clients" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-user-pin mr-2"></i>Clients <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-clients">

                                <a href="{{ route('panel.clients.index') }}" class="dropdown-item">People</a>
                                <a href="chat.html" class="dropdown-item">Properties</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-work" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-layout mr-2"></i>Work <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topnav-work">
                                <a href="layouts-vertical.html" class="dropdown-item">Overview</a>
                                <a href="layouts-topbar-light.html" class="dropdown-item">Requsts</a>
                                <a href="layouts-boxed-width.html" class="dropdown-item">Quotes</a>
                                <a href="layouts-preloader.html" class="dropdown-item">Jobs</a>
                                <a href="layouts-colored-header.html" class="dropdown-item">Invoices</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link arrow-none" href="#" role="link"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="bx bxs-report mr-2"></i>Reports
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link arrow-none" href="#" role="link"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-time-five mr-2"></i>Time sheet
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>
