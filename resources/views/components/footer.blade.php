<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                2020 © Grizzlydesk.com
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    All rights reserved
                </div>
            </div>
        </div>
    </div>
</footer>
