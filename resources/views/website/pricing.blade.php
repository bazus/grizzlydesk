@extends('layouts.page')

@section('body.attr') data-layout="horizontal" @endsection

@push('page')
    <x-navbar type="nav-sticky"/>

    <div class="" style="
        background-image: url('https://images.unsplash.com/photo-1505798577917-a65157d3320a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80');
        background-size: auto auto;
        background-position: center;
        background-repeat: no-repeat;
        margin-top: 60px;
        padding: 0;
        ">
        <div class="jumbotron" style="
            background: rgba(0,0,0,0.5);
            color: white;
            ">
            <div class="row">
                <div class="col-md-8 col-lg-6 offset-lg-2 offset-md-1">
                    <h1 class="text-white">Try grizzlydesk free now.<br>Pick a plan later.</h1>
                    <p class="lead">14-days free. No credit card required</p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="{{ route('auth.page.register') }}" role="button">Start free trial</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

            <div class="container-fluid">


                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="text-center mb-5">
                            <h4>Choose your Pricing plan</h4>
                            <p class="text-muted">To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words If several languages coalesce</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-md-4">
                        <div class="card plan-box">
                            <div class="card-body p-4">
                                <div class="media">
                                    <div class="media-body text-center">
                                        <h3>Core</h3>
                                        <p class="text-muted">Annual Plan</p>
                                    </div>
                                    <div class="ml-3">
                                        <i class="bx bx-walk h1 text-primary"></i>
                                    </div>
                                </div>
                                <div class="py-4 text-center">
                                    <h2><sup><small>$</small></sup> 29/ <span class="font-size-13">Per month</span></h2>
                                </div>
                                <div class="text-center plan-btn">
                                    <a href="#" class="btn btn-primary btn-lg btn-block waves-effect waves-light">Sign up Now</a>
                                </div>

                                <div class="plan-features mt-5">
                                    <p><i class="bx bx-plus text-primary mr-2"></i> <b>1 user</b></p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> 1-on 1 product support</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client manager (CRM)</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Quoting and invoicing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Scheduling and notes/attachments</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Mobile app</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Credit card processing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Reporting</p>
                                    <div class="text-muted">
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Client hub</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Client notifications</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Invoice follow-up</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Routing</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Live GPS tracking</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Time tracking and expense tracking</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Job forms</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Online booking</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Email and postcard marketing</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Facebook and Instagram ads</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Task automation</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Quote follow-ups</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4">
                        <div class="card plan-box">
                            <div class="card-body p-4">
                                <div class="media">
                                    <div class="media-body text-center">
                                        <h3>Connect</h3>
                                        <p class="text-muted">Annual Plan</p>
                                    </div>
                                    <div class="ml-3">
                                        <i class="bx bx-walk h1 text-primary"></i>
                                    </div>
                                </div>
                                <div class="py-4 text-center">
                                    <h2><sup><small>$</small></sup> 99/ <span class="font-size-13">Per month</span></h2>
                                </div>
                                <div class="text-center plan-btn">
                                    <a href="#" class="btn btn-primary btn-lg btn-block waves-effect waves-light">Sign up Now</a>
                                </div>

                                <div class="plan-features mt-5">
                                    <p><i class="bx bx-plus text-primary mr-2"></i> <b>1 user</b></p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> 1-on 1 product support</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client manager (CRM)</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Quoting and invoicing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Scheduling and notes/attachments</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Mobile app</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Credit card processing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Reporting</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client hub</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client notifications</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Invoice follow-up</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Routing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Live GPS tracking</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Time tracking and expense tracking</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Job forms</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Online booking</p>
                                    <div class="text-muted">
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Email and postcard marketing</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Facebook and Instagram ads</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Task automation</p>
                                        <p><i class="bx bx-minus text-primary mr-2"></i> Quote follow-ups</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4">
                        <div class="card plan-box">
                            <div class="card-body p-4">
                                <div class="media">
                                    <div class="media-body text-center">
                                        <h3>Grow</h3>
                                        <p class="text-muted">Annual Plan</p>
                                    </div>
                                    <div class="ml-3">
                                        <i class="bx bx-walk h1 text-primary"></i>
                                    </div>
                                </div>
                                <div class="py-4 text-center">
                                    <h2><sup><small>$</small></sup> 199/ <span class="font-size-13">Per month</span></h2>
                                </div>
                                <div class="text-center plan-btn">
                                    <a href="#" class="btn btn-primary btn-lg btn-block waves-effect waves-light">Sign up Now</a>
                                </div>

                                <div class="plan-features mt-5">
                                    <p><i class="bx bx-plus text-primary mr-2"></i> <b>1 user</b></p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> 1-on 1 product support</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client manager (CRM)</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Quoting and invoicing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Scheduling and notes/attachments</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Mobile app</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Credit card processing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Reporting</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client hub</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Client notifications</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Invoice follow-up</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Routing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Live GPS tracking</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Time tracking and expense tracking</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Job forms</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Online booking</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Email and postcard marketing</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Facebook and Instagram ads</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Task automation</p>
                                    <p><i class="bx bx-plus text-primary mr-2"></i> Quote follow-ups</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <x-landing-footer/>
@endpush

