@extends('layouts.page')

@push('links')
    <link rel="stylesheet" href="{{ URL::asset('assets/libs/owl.carousel/owl.carousel.min.css')}}">

@endpush

@section('body.attr')
    data-spy="scroll" data-target="#topnav-menu" data-offset="60"
@endsection

@push('page')
        <x-navbar type="nav-sticky"/>

        <!-- hero section start -->
        <section class="section hero-section bg-ico-hero" id="home">
            <div class="bg-overlay bg-primary"></div>
            <div class="container centered-item">
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <div class="text-white-50">
                            <h1 class="text-white font-weight-semibold mb-3 hero-title">The easiest way to grow your service business </h1>
                            <p class="font-size-14">Grizzlydesk`s award-winning software helps small home services businesses organize their entire operations, from scheduling jobs and managing their crews, to invoicing customers and collecting payments.</p>

                            <div class="button-items mt-4">
                                <a href="#" class="btn btn-success btn-lg btn-block btn-extra-large">TRY FOR FREE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 d-none d-lg-block col-md-8 col-sm-10 ml-lg-auto">
                        <div class="card overflow-hidden mb-0 mt-5 mt-lg-0 plumbing-image">
                            <img src="{{ asset('assets/images/add/plumbing_software.png') }}" alt="" class="img-fluid d-block">
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </section>
        <!-- hero section end -->


        <!-- Features start -->
        <section class="section" id="features">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mb-5">
                            <div class="small-title">Features</div>
                            <h4>WORK FROM ANYWHERE</h4>
                            <p>Grizzlydesk has been designed so that, whether you’re at the office or in the field, you have the tools at your fingertips to organize your team, communicate with your customers, and grow your business.</p>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row align-items-center pt-4">
                    <div class="col-md-6 col-sm-8">
                        <div>
                            <img src="https://images.unsplash.com/photo-1586892477838-2b96e85e0f96?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=100" alt="" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                    <div class="col-md-5 ml-auto">
                        <div class="mt-4 mt-md-auto">
                            <div class="d-flex align-items-center mb-2">
                                <div class="features-number font-weight-semibold display-4 mr-3">01</div>
                                <h4 class="mb-0">ORGANIZE YOUR OPERATIONS</h4>
                            </div>
                            <p class="text-muted">Run your entire operations and manage your team from one place. Grizzlydesks’s client manager (CRM), scheduling, and invoicing will make your processes more efficient.</p>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row align-items-center mt-5 pt-md-5">
                    <div class="col-md-5">
                        <div class="mt-4 mt-md-0">
                            <div class="d-flex align-items-center mb-2">
                                <div class="features-number font-weight-semibold display-4 mr-3">02</div>
                                <h4 class="mb-0">IMPRESS YOUR CUSTOMERS</h4>
                            </div>
                            <p class="text-muted">Provide a convenient experience that keeps customers coming back. Grizzlydesk’s online booking, self-serve online portal, and automated client notifications make you look like a pro.</p>

                        </div>
                    </div>
                    <div class="col-md-6  col-sm-8 ml-md-auto">
                        <div class="mt-4 mr-md-0">
                            <img src="https://images.unsplash.com/photo-1557426272-fc759fdf7a8d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=100" alt="" class="img-fluid mx-auto d-block">
                        </div>
                    </div>

                </div>
                <!-- end row -->

                <div class="row align-items-center pt-4">
                    <div class="col-md-6 col-sm-8">
                        <div>
                            <img src="https://images.unsplash.com/photo-1526628953301-3e589a6a8b74?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=100" alt="" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                    <div class="col-md-5 ml-auto">
                        <div class="mt-4 mt-md-auto">
                            <div class="d-flex align-items-center mb-2">
                                <div class="features-number font-weight-semibold display-4 mr-3">03</div>
                                <h4 class="mb-0">GROW YOUR BUSINESS</h4>
                            </div>
                            <p class="text-muted">Watch your business grow when you automate your sales and marketing, and provide faster payment options. Grizzlydesk’s 20+ smart reports tell you exactly how your business is performing.</p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end container -->
        </section>
        <!-- Features end -->

        <!-- Team start -->
        <section class="section bg-white" id="industries">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mb-5">
                            <div class="small-title">WHO USES GRIZZLYDESK?</div>
                            <h4>WE SERVE 50+ INDUSTRIES</h4>
                            <p>No matter what industry you work in, Jobber’s field service management software is customizable to fit your process. Schedule jobs more efficiently, optimize routes, send quotes and invoices via text messages, and get paid on the spot. With a mobile app, 1-on-1 training, and simple setup, you’ll be running a more efficient business in no time.</p>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="col-lg-12">
                    <div class="owl-carousel owl-theme events navs-carousel" id="team-carousel">
                        <div class="item">
                            <div class="card text-center team-box">
                                <div class="card-body">
                                    <div>
                                        <img src="https://i.pinimg.com/originals/e2/07/09/e207097b532d3edd84b164a5a3f3eed6.jpg" alt="" class="rounded">
                                    </div>

                                    <div class="mt-3">
                                        <h4>Landscaping</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card text-center team-box">
                                <div class="card-body">
                                    <div>
                                        <img src="https://capital.com//files/imgs/glossary/800x600x1/Clearing-balance-requirements.jpg" alt="" class="rounded">
                                    </div>

                                    <div class="mt-3">
                                        <h4>Cleaning</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card text-center team-box">
                                <div class="card-body">
                                    <div>
                                        <img src="https://signaturetreecare.com/wp-content/uploads/large_tree-trimming-and-pruning_signature-tree-care-1-800x600.jpg" alt="" class="rounded">
                                    </div>
                                    <div class="mt-3">
                                        <h4>Tree care</h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="item">
                            <div class="card text-center team-box">
                                <div class="card-body">
                                    <div>
                                        <img src="https://www.suihvac.com/wp-content/uploads/2019/07/ServiceUnlimited_May-blog-images_0000s_0001_Layer-3.jpg" alt="" class="rounded">
                                    </div>

                                    <div class="mt-3">
                                        <h4>HVAC</h4>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="item">
                            <div class="card text-center team-box">
                                <div class="card-body">
                                    <div>
                                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSExIWFRUXFxUXFRUYGBUYGRgXFRUWFxcWFxUYHSggGB0lGxUWITEhJikrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lHyUtLS0tKy0tLSstLS0tLS0tLS4vLS0tLS0vLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcAAQj/xABIEAACAAMFBAYHBQQIBgMAAAABAgADEQQFEiExBkFRYRMiMnGBkQdCUqGxwdEUI4KS8GJysuEVFjNTc6LS8SQlNFSTwhdDs//EABsBAAIDAQEBAAAAAAAAAAAAAAABAgQFAwYH/8QANREAAgIBAwIDBQcCBwAAAAAAAAECEQMEITESQQUTUTJhcYGxIjRCkaHR8BQzBiM1coLB4f/aAAwDAQACEQMRAD8A1z7VM/7eZ5y/9ULFrf8AuZg78PyaCMcYYAydbiNJbnuFYQt4E+oYnTchSI1gpmOcOiNiPtjezChbD7MT+jEd0Yg2GQRaj7MctqPsxO6MR3RiDYCJ9pPCPftH7MS+jEdgEGwEUWjl74ULRyPnEjAI7AIWwbjHTjgfdHnTjgfdEjoxHdGIewbkY2gcD7ojWq80TI1rBHoxDU6yq2oEGwgOb+l8/KEHaGVvJ8oKG75fsjyhBuuX7A8odIVgwbSyt7UhX9ZpAzL5RC2rtVnskquBTMYHo1oPzHkPfGTW6+pjYqnETkBkADxyyizh0zyb9jlkzdOy5NBvj0jUOGzywf2nrQ9yinxgN/8AJFtBzWSRwCN8ccUSdOO+PBNjRjpMaXBVeab7mo2D0oDITpBHFkNfHCfrB+Vt1YmFemp3q/lpGIrOHCkOCaK1Bz8qxCehxvjYlHPNG9ydoLO4xLNRhxB90Of0xKPrr5xkmz15CWExpiQsciNc86GNNstx2eaizFXJgCPGMuUEpNejL1uk/UnC95R9dfOPTesv21/MIHWnZyzopdhQAVMVgqoPYFPL3iOmPB1p0cp5lDkvIvOX7S/mEKF4Sz6w8xFRsL2ZmwvVCADWpIz48INJYLPSvSrQb8QhSwuPILKpcMK/bE9qviIWLQOPwiuWi0WVNCzfur82pEFr1lVylGn7w+FIa00nwhPPBcsuQnCOiof0jZz/AHg5UX6x0H9NP0H58PUv0eM1I9iFbZvqjxiskdmInTgYbs+URpkP3Xa0mpVGBwkow3qy6qeB0PcQYmRJv2pBq6jvIHxjheMn+9l/nX6wA2lsdRjpqKHv3e74RRVl505wqGjWhb5R0mp+ZfrCxaUPrr5iM0u+XnFksMuFQy1CYOI849xDjEGzrlEpRBQWO1jo8EexEZ0dHRxgAjWy8JMkAzZsuWDoXdVr3YjEP+sliOQtln/80r/VFD9JM3ppol6iWP8AMYokux0JNNAYn0iN0O0ti/7yz/8All/WETdqLGoJFqksQCcKzEZjQVoFBqTGDCSTU0yFBXmc4cFEBPHL5n5R2xYuuSRzm+lWTdor2e0zWmuczoNwA0Ud0BZxpQQ5MmZxHmnONyEVFUjPbtnOf184QRHuKEs0TBCi+VIQWp3ww86PJZ3kwrJdJZrjt+FWUkEAYgpOvGnPSNA2N2ykInQTpoQAno2fIUJ0LaDOusYvhdmbBUnDlTcFzY07oN3FdoeWrTiSrMQAN9Dn4RkarH9tyS9C/hpwUWbRtHbw9ERgVyYkGoJ3UI1FIrs+YEUs2SqCT3DMxwtqgqgG4aaAaAQG2mtJJl2ddXbE/wC4p08Wp5GLmLH0pRRmZJ9TbH7HM6uJu05xkd+g8BQeEPGdXdHkqzECp0A1PyEKQ1PVXxP0jtsV/tHnTtHomH9UiSEA1p5fOH0l8chCbXoTUX3YPDHgI6C4ReEdEb9w695f7RMwisDC0Trw7HiIGgxhrg2HyeTdDAbY+wTZc+e7ScEtxUNl1ziyY5k9mvCDDxLu1/u0rrT/AGhjRJmygylTocjGa3rYjKmsnA5HiI00RRtoRinPXjl5QgIV3tQxZrBMHGKjmN8LFtcQwNDlzF4iH0ccYzF7+KmhJXvyHmcjBCRb5zLjBWn+LKHuLQUBoYccY9xDjGaDaSYDTM9xBHmDSFjaaZwPuhdI7NIrCJs0AZkRmdr2xMsVfEATQUzJPCkRbTtYSSpxVy97YR74OkAjtBYVxswfEWJO7fFcnWcKrQ9ab166qwPWyBypwz8SB4iFvnuiaEQLBZcKUI1qT4/yAgNtDJCMtMgQT48otOGK/tSVIUV6wqaciP5Ra0v91HHN7JXsWcIZo8aENGwikevMMNuSY9rHhNYGSQgJ/OHhLLURFLMdAASdKnIcgTCS3CI/SuDRWKlgy1BoaMpVhX90mISdLYmlb3JtltKpJeh+8dsJyphlpQ6/tNr+5zi1XbbjKsko4MyDlU1FSSDTxikOqgotOrw5CL1ctwtOVZs04VyKUrUjuOQEZ+X7WTo+bLMWoQc38ghcheY7EihBGLll/MQcFmUOXCAuaAudaDQV4cuZj2zykljCo5k8TxPOHemHGLjdmQk0cV4wggAZQozRCVNc/dCHuKkSfWOfAcIkUhkGF9NCYxX4Y8juljyAZe7x7HiIFiCl4jqHw+MDFjDXBsPkS8BJ9pZJhKsRQ5f7QbcxXbefvG74YB6zbQpQdICDxGY+oiv3vOV5rMpqCcj4c4jMYbcwhjbwiOZoI3E8gvhnDI0wtuB4Ny5wAVHaSxGZgJxYBqVNCGZ5YBpv6pmbjrBH+q9n+xq7PMXrtUhFBC5hczLyrQQR9MNvFllSESUGE16M1aMMFGAXdnQ5nhzqA997a/8ALAi2S0YukwY26Low6qzlSyuTXCpOmgMc5TmpJRVqicYxatskWaRhRVoRRVHWNTkKZmFmXCrstQnyJU4Lh6RFcitaFhpWPbWxCmmug7zkPr4R0IgRpfS2j9iV72/3H+WIVpP/ABCrxKD/ADhvkYOyEWVLqd+fM8PGnzgSLuM5yxoMXy/QhoCVeckTJZwkFl6woQTlrTnT3gRIuu1icgcHPRqbmoCR7/IiI77PpSqjMZimvhziPca9DMMr1XqQdevmdeYHuESF2J17W8ShQdojIcOZin2iZViSSTxidfExummV9ojwGnupAthGzp8ShH3soZJOUhqZvhAEPlaiEBYskBsR6qVi03VsZNmKGmBkroKVNOfCJR2ekpkSSdOsae6Kk9bijsnZZjpcj3exVrNdrzDRFJ56KO8wi1XaZTZmpIploOIjTLrn9BKVVkdMxYjMrhRd1FOvhU5wq0WazWyWWVUR8wcK4GBGoZSBUbozpeJPr42NCHh66OfmZFJsjTZwUDKoB/drmY1yfekqRLXFWlMqDKm7OKpYElyndaYGBpTUdwP63Q7brun2xwkodFKlkq05iesd4lqNaZ56Rb0yuPXLvuZmrf8AmdC4iFrJtIk1sKy3bmoBFIOrLqAdDwO6BtgsC2dBLlCp3sdSeMSpVmmakxaddiluPiXxHjClYbo5AQKEw05wwhMdOcIEsRDa0MdI9QuYlQrJmHnHRHCtxjoVDNJt/YMCBBi3dgwHEYMeDZfIlzFft8usxtdfkIPvAa1r1z+t0SAHtI/WcNPZxBLBEaaIQwe8gcIXZJSg5gQ8VjwjCCToBUngIABm29wWq8llrImSgZRqFmOUOhFE6pBHKo07ozva+6LxsktLPapqBWczFRXVjjKFekqFroSNd8WfabaWbKnS/s8x0FGrSlDTPNTUHIbxEHa151qssq0zmD/eGWG6oYAywRkqqMIpw390AcFr2dtBmWWQ5CgmUuSigyFBQbshHluOKYiVoK1buAJNfAEfjikWH0gtKSVKNmBCKqkhwtQMqqgWgy3Vh227a4ZpcWct1aCrhciQa5KfZHlCtXQy5OhmNy+UIsqfetyyH5RFN/8AkCaOzZVHe7H4KIiHbi1Biyy5IruKzDupriHCGKjVZaACpgPbLCWbEikdYMtRoQa/GM8tW29uenWlrT2U/wBRMXLYvb8TZiybUFVmoEmDJWOgVx6pO41oeWVVKUlwdccYPnk92luh2PTIprTrLnXLeOOWsVYiN0+zKwgPbtkZEw4jLWu8jEtfykRZweIOEemav4HPLo1J3B0ZFTjCJ97CxzZMwdDOr1mUMHwUNM8JybfGpTtiLMRQygRwPW866xGkbDWOVNE5ZIV1NVIyAO44OzXnSDN4n1rpimkGPROD6rVk649pelYL0YAIqHByburyiZeFilTTV1z8oFyLmkyRSXLVKaYQBDhtBEZnUaV+4KWKUkpcK6R7NtMpFZnFRmT1SSeQpp4QJmWogVIIEQLXbSRQGHT7B1AK5S020s7LQTHY4SBklNBXuEXtVoIAXVdTdIsx8gKkDeTxPDWD7vGzo23jVmF4hSyuu44ojnYCEK8JmGLVFLsMzZsRixO7KHmIhBmR0RFjZUDuhzHwyHGEFqw1ioePf8TDIvYkYo6GOk5nwGUdBQdRqFsHUPdAkJBe1dhu6BStGBHg23yNvKMCbWlHPh8INmZEmzmorDEVfByhiZZnOiMe5TF1BgZeV6PKagVSNxNfkYBlcW75p/8Aqf8AI30gldl0lsUudKbo3ArkR2TUAnUDu5Q4l/zWYKFTMgaNv/FFjVjTOEwMc9Md3y5U2yCWiooSdVVAFefM84hNI6e7JcoDrNaVVd1WcKqgV0zME/TbNHT2cUrSVN99cvdAp5xF2K65N0/VG8FQCCOcTQitbR7PJJfBLmyWKhcSpiopq4whyKORQV+cQrnaWlpVbRLLpTrqNezlQ94iRMKPNZEZlEsAM4GKrUqQRrrUVEWe7kS7rMJ0+cAXKdIq1YYnUMMJHBSKkVBqDXSF7xvZjFpeysPurvmtwP3h9wEV287rZiKWfogTQVxCpOgq0aHYLck4B5bBlOhH615QYs1yzv7SgWpNM8yO6hhORKMbfBlVk2JtUxVZZS4WoQxdAKHfStaeEWa6/RcKq8+0igIJSUu8Z06Rt1f2RFzWUwNGBGfMfCKtt/a2kfZRKZlxzqMAzdZRh6pz060QldHWHTdVuaFLnIgpWEteI0GfdUxWrir1g2ehFc+NflBtBCWIUs/uHpk52yA8yIYewTzoE/N9REiVrBKzQeTEj/UTKvabFaF7UpvCjfw1gfLQk5xoJMMzpSt2lB7wDCeBdia1L7orkiSCtCK8oS1nlywXwgUFSaVPhB1rOoGS084rlvn43CrkoOvEx1w6RzlXbuQza2MI3W/YhSryWbR5ZJUjqjSo9oV1rChMavZJ8RDBlrXCerTQer4cI6ZbAmTYu/o5hHmARG7CCilGJgzyOcnKRPSa3ADuhMx4GtfEse0fw0/iIiNM2gA7KqObGp8hD6GJZIruFwjHdDEydLQ0LYm9kZnyEBZt748j0kwn1UBA8SMvMxIsy2gjqSkkjies3098S6SDlYSM9iOyEXixz8obFW0z50yhoWZVzmPiPFjXyUZQ3ar5A6qCp/W6CvQV+o+XPGOiuzL3NTSpz1GnhHRLpFv6G+WgdVu4wGWDU7snuMAwY85E32esYl2Q9UePxiC5hyy2oA4DrDAIgwA2iYYl7jBwNADaCSQ4bcRTygAGWdwHUnQMCfOLq00BcROVNYopix/0tJaXhYkZUIoa+BEAzMPS3NLzJcxWBTrIARvyatCKbjnEKy3pLaxKjNRhMY0UNRiEUhSw0PjkDD/pPdQksLWmNqVpWgUcIr9lNLLKOX9q5odMlXXlDoTWxBUl2xZAHEaigIq1T2ezu5UEded9zwiy0ZqtSaWfrZElZZRTUDqjWlak+LV5zkl2dmDffLMlhFw5YCswvMBpTtKq0GlTyi4be7KJZ7NJmoQpMuRJK7iUSY5cHcTV69wgfoMl+jOytO+9VAktmINTrMFASvEajOmYA3RrUtCop4RjPoxv1jMSxvQImJ1ca5mpQjkWJqNwjaqc6xwknZa6k0mQLRIrkYqe1Gy4tBktiIMmYHXgRiQspHMIKHdFztIIziPhrHBtp7HWL7lfk2cK2XMeG6J8lKxJtFkBzGRGhhmU+E9YU57v5RYhmT2ZVnga3juPypOcTpS0huUARUZjlD6rHcrntI8Kw7SK9fd7ay5Z5Mw+A+sTxwc3SITmoq2NXxfCFWSWwYgkORoKaiu87vOA0hgStDvED7nAEoDvr34jX31iXIkUmKw8Y1oYljVIzMmVzdk61yAc/OIySCvZYge6CZziO8mJJkHQyv7QB8BDvRqNEXyEd0QhSwMaojznbdQQOn4zqxgnaHAiibUbXKlZcnrNvb1R47/CGnQ3BydIkXveCS8sQrxJ0iuf0g8w4QGVDqxVji/Eug5QMsXSzWxMcVdx08OEXK6LuUDRkPEH9Vjqt1YpqOHblntnSWFA6SVpvcg+VY6Cv2BvalnmZa18aER7Dsr0japmh7oBAQebSAceaiejY04gVbWo/gILtAe9e2O75mJCJ1gvH1WPcYJWmSs1Cp8DwMVVXgnd9tIyJy3QgIFssTyzmMvaGn8oixc0eohPQprhWvcIAMQ9J8/OTLpuZ695w/8ArAF5pFkQBWJxua4SAOqvrHI5Z5Vi9+nKRL/4RytGPTKWAzwr0ZAOYqAWP5jFONFs6kCqh9+8sQACK8jlwrDsLClw+j6Zb7PKnCYuHp3SYjFlpLlzCGKMAa5VFDTTXgj00W92tXQkEJKwrKWuVDLR2mU3mrYM9BLFNTGj+i6UUuuQG7RM5j3tPmRme0U1LTe9oYnEq0VKUYMVkrprXQeUKh2VfZibNWarIxDKy0IyPW6uveQO4kR9D2HaOUkofaCVZJZZ2CsQcCgsQFBIO+lIxrZC66TJpYVKI5Fag1rQGmmmfKgpvi/2gBlHBlcfmltA4klJovb3jJmJQPrp1W94IiOZlVrQim6AVgFacaCCihhEJYkxxzOLH0eEzVBhsAjMeIh0nKsVJwa5LkJJ7oZWzbxl3Gnwh1RMGjnxz+MOS2hwNSEpNcMbSfKBF8Xm9DKyBPaIqMuEAEzy3iDO0FmIPSAVBoG5HQGBJWPQ6JxeJNfP4mBrE1lafy+AOOZKrk4zodGBJpQ7ju8IZS8nlmjKRxBELviyFkJUdYA0490DpF8dFLAmsr6YcRAPdUxoLdGbJUw/LvlTxEPy7YG0cGKq21EsayG71KH4wlNuLODQrNB0pRNeGUJxonFSfvLj0ZO+FpKIyFSToBrEvZ+wtOlia8tpQbMK/bpuJX1a8DnFhk2ZU7I7zv8AOKeXUxjsty3i08nu9jN/SJc1rFheZJNCuc1F7XRUOIqRvGpA3V4RiNktLIcsx7J0/lH1u4jEdu/Ru0qY0+yriksSzSl7Uo6mg9ZO7Md2cVY5JTkrZejGMY1QM2dviztQOMBi9WUIR1SCIy+y3ZUaQXsUmdKzRiOWo8o10pVuZGaGPquL/M0DohHRV0v+aBQy6nkY8go50/T9UfQZgE2pg6YAzNT3n4x5yJvsS5gPenaHd8zBSY0CbyOY7vnEgIhYeekLRobMeGYBvhAWC7rTUUgiGirWO2KD2h5iDD3gABhKnxr8IYGYem6Q32iRMIbAZWFSDljV3LVyO514a8jFLvBwbMKVBBbeORqMss417bOzC2yDIYZgh5bBW6rioBqdxBIPIxl1mwyDMsttlOqtSjqBjlsK0mLWgmKQSCK5jeCIKBGlWSeky5GlSZ0osbNOQfeIKM4fqs1eqetTOMYuZZiTFdCAwZSF0IYHIZd3lBX+rEitenlzFBqGUlGpwKOAa/qsJNhlLMXoQWOgQdZid2gEOhhO67HPtNvE53ZqOZjYVKqMQNcRrvBIpvzHGLpLd+jVfWTGpGWZEtwhz3EUPjHuzVztZpNGIExzicUrQ0oFrXd8zA/bO3vZrOZiUxu3RhvZqrnEOYoad/hEW63HGLnJRQZ2amPJQSGUu0pRoUqU3YRkHA0qK6Qfst+SMI67eKPXjnhWkDZdnxoMYxCgHWAJpwrSCMqQhABRTTSorTuhkXQqbtDZhkWY8hKm/HDT3wzct8dJ92VYEdljh6w/CTTxibLs0saS0H4V+kQZiYZ+Ibz/ABDP3wpRtbjhPpdoO4KiIzVXfUQ7JmVGsD7bPZGANDiNFoak/h1ipkwtcFzHmi+SZjBFDmDkRAK2WMyz+zuPDkfrBMAnQ0PA6wiaDhOPOsS0uolglfbug1GmjmjXfsVW87aFBFc6Z0pFKvFpDNWYjPvp6vfSLZeN0DGesxU+rXMHv1iPdlzKjMxzevVr6q8QOPOPUYMkcmNTjwzyWd+TneKT+0vT9ytS9nOkOMBpKGnUrn30PZ7oN3JdQshLySFmH1yFZh+6zg4fCkWGTYqvhocJ1b6cu+JkuzqpyUVqQSa5+J18KQTnHirIx857uTSIEu9LSRX7TMPl8hSJMq+LXTEJxYcwh+UTsGGgFAK+qPjrCZjHTFTPgPpFWXS/wouY1Jfif5sjrtNaRXF0ZpxUjwyML/rS/rSR3q3yIh2ZLqM8+eEQ21mFAAqkUNcvhSIdEO8To5ZFxIAXzPss2jiU8qYTVmAFCM6kqDmdM8jAtrRLUgYgwO+jDwIIyi0zLCuQMtcNNQSCD5RBnXNKpXGR3rXfplWsWcc4x23KuWE57tJ/oCAZZzqI6JjXC9erMl4d1RQ+Ij2Ovmx9Sv5E/Q22AU8dZu8wcBgJau23fHnonpGRZsCbySpXx3n5QWmwNtu7x+USAHiQOHnnC1kL7I8hDgEKAgA6WtIehKiFQAeNFV9IFlnTbOqyJImPjBrWWMIANe2Rrlpwi0tDUxaiGIq9n2MsuFS0tg1BiAdqYqCu/jBi77tkyB91KVD7WrfmNTBNZeUeiTAOyIQTEG+9m1tksS5juoDBgVw1qARniByoxg6kkQ+iwqvkFJxdoTZrPhULUmgAqaVNBSpoKViQsoQpBDoEMiN4YF3o4UlqHqgHTWmeXGDWCGLVaJcqhmTFSumJgK04V1gAE2a2T5ygyk6NDozjrnnhOS+/wibYbtwtjYl39pjWncN0MnaGyA/26k8gx+AgjYLbLnLjlOHXSo3HgRqDyMIY8ZAbXz3juMRp13O2WMU40NfLSJohQMRlCMuUTjklHhlUvSzIkzAvqgYmJqWY5knwpEN5NcxkRoYnXiaznPOnlEZjHoMKrHGK9Dw+vzNaieRve3+g/ZmVlFcipoyivWNd54Q5MbOvhTWIaHOvn8ol4MWfCOLXS6NXBlWfGsi7/UcmyjTOkIRarQnPTxESZTYh7oYMuhIy4b/D6RzLSFKgoDUZ7s4T0dNCIXLAGROR0zOVdR5wtlI50hEiK6HgIQ0sbx5RKccqe6GmzhMlEYNP0Y6FhOPwjoVkzRYC23tt3wagLeHbPh8IzImgyHNgda93jE+aYH2gxIBoR6BHghUAz0R7CYUIBHtI9wxwiBf1sWVJYk0LVVacSDA2krYJNukE0WF4YhXNaxNlI4NcqGvEZGCAECdq0DVOmJCw4oiFbrxWUQGVzUV6oU+dWECr72pSXIdpVelXCQrI+YxDFpkThxZV3QxFje0IhAZ1UnQMyivdUxIlkEVBBHEZjzjGtoNpJFt6GZ0aGehAxiaURVLEjED2U0JLVIzGmcGPR3erfbpslpqFCpCCWfu3dTWqse0aVz5GkKySg3wjURFZ232fe1KrKyrgDFsVeyATlQHPWLQBHk2WGUqdGBB7iKGAiYVikS8WGe5alKqlNQQcy2YIMEtltrFskypV2RhRwMNTTQhSRnWu/fAjae6mstpeU2e9W9pTo2XHfwNRugdZmWtXBIArlQknhQkfEb89xreZK6PVLwvSrF5n2pbXtu/lVG/3Ne8m1y+kkviFaEaMp4Mp0icRGaeidgZkxlBQMFBUkHNS28b841ErFiLdbnm86xqb8u+ntfJUbxFJzjmCPECIr6wZv6RR1fiKeX+8CFlksANSaDvjewTUsafuPnvjMJQzSj6v67iKRMsrV507XduiPSGZjlDjVsLaZ5gjM4WHnBmVqyHgmoccvlPiX1X/AJ/0GUl0P6pTjDk+VXMb8q5a7jFTtG1JTqBcUw6S161K+ti9Ud8Iu7ayYD0dplha5rMQ9UfssD8dIpPJG6s9csMquizNKNM6g19/84UKFagnLd9eYj0OGUPUZ0rTTkYZnKQcQ138/wCcTIDvRjiYSy0+sIkzcQBr+t4pDj6fr3wmNDBHdHQoUjojR0sv8Br07Z7hBmAt79vwHzjNRoA+cYHzjnEycYGzZlG90SYIcEe1hIMe1hDPYUIQIUIAEWy1pKltNmNhRAWY8APj3RlW1G1j2hlZVwIFqoNC3WoandUimQ4b4J+lG+WJFkU0QBXm8WbtKvJRkeZ/dzod7TKNTgFHkoHyhN70dIwXluXvS/O/2LJcO3M+zMFwI8oVLrmGIFACrVIBqRu3GLTZvSWXmhBZeod5mUYZZk9WnHL3xlNjNS3LCPcxPygtYpmHpG/Zwj8dQf8ALigbdbBjinJXx3+C3f6Gz2S0vPxWizzxOkOowy8KHAwAqpJGJTvIOnDPILbw5mOZsro1IXqjPCABUqRkanxHjGeXNeM6zP0khzLbIHgwG5lOTD4VypGgSdoLNekk2S1gSprUwmvVLjstLY6GvqNrpnEndbHOg5YdibtaV93IUo+eINMqc60xYqih3bqRUNrNjZlkPTyCzSgQa+vLNcqkbq6MPHcTBlXnb7mcySFZCariDMjqNcBDDCdMtRlujSdk9rrPeC4BRJtDjkNQkj1ih0mL7xXMCISj1clzQ6yeln1R3T5XqV/ZP0gVpKtmRyAnAf8A6AfxDxGpjRUIIBBBBzBGYIOhB3xmW2mwxl1n2ZSZeZeWNU4leK8tR3aB9k9r5tjIRvvJNc0J7NdSh3HfTQ8tYipOOzNjP4Zh1ePz9G9+8f5w/wBH2Lf6Vbk6Wzi0KOvJ15y2OfkaHuLRlF22TppsuWZhlhnQYxnTMbo+grBbpFskkowmS3BVxvGIUKsu40MYNfl3PZLVMk1NZb9VuI1VvEEHxjnlVNSRPwrJKeKemns1fPv/AGf1NwuTZ5ZDdJ0jOxAFTTTnTWDmGBWyt5C02WVNG9RXkwyI8wYLxYuzzE4uMnGXKB182fFLPFet5a+6K46GpIGYGMfhOfzMXQiusVGfLMqYcq4DU81OR9xEaOiybOJ5rx7Tr7OX5P8An5/kRL0nMkw5CjdcEaUMDrXOMxaH9Ug3edmLywFFTKOf+G2anyPugeLpm+xSIZZZbpcHbQY9H0LJJJT+Pf8Am3xTBMmxqooqha5mnz4xHtFirkMzFss2z5NMTeA+sT5dwIjYkZge+scFjfc2HnjW25nNms9tlzEWUz4QTUEno1BNGDJvJzy8YvVpQS1UgkhmACndUE667oKMjBczXnvis2naiyPaZdjE770FgQVYAOAKJiYAFjiNAK6d1ZzbglTOcKyt9SJ02Qa1UjdUHQ/QwkMRxpDztSpOQrqcoFW3aSxS69JapII1AdWb8qkmCOetpDlp73iTi0dFYfbu7a/9Q3hLnU/hjon58PUj5EvQ2iA19dsd3zjo6KSLYInQDvA5+MdHQMaJ4jo6OhAeiFiOjoYGJ7ZMTbbRX+8I8AAAPKAd9H7xv3jHR0Rftfz3FiP9h/GP0kIun1/3jBOy9n8f/oY9jofY54+X8H9B8Q1OGcdHRMgaNXpLgLTOuyoSrN1iCs0qCCdCBl3RlrTWSrqxVlBZWBIIYaEEZgjjHR0IXdn1BdTkyZRJJJloSTmSSoqSeMYxt1KVbdaAqhRiGQAAzVSchzNY6OjnLg9F/h37xP8A2v6oJ+i2YRbKVNDKaoqaGhWlRvhHphH/ABsvnISv55o+Qjo6OP4DQzf6r/xLR6H2P2VxXSYacshF+jo6O0PZR5jxH71k+LOivX1/1A5yzXnk0dHRc0nt/Jnn/F/uz+MfqjrF8bOK8+1rEuR2V7h8BHsdFjJyzF0vso9sp7XfEubvjo6OM/aN2HskS0dmPm30gD/mNq/xB/Akex0c83sL4ljTe0ytmez9tmammIk07qxxjo6KxcEiPI6OgEf/2Q==" alt="" class="rounded">
                                    </div>

                                    <div class="mt-3">
                                        <h4>Plumbing </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </section>
        <!-- Team end -->



        <x-landing-footer/>


@endpush

@push('scripts')

        <script src="{{ URL::asset('assets/libs/jquery.easing/jquery.easing.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{ URL::asset('assets/libs/jquery-countdown/jquery-countdown.min.js')}}"></script>

        <!-- owl.carousel js -->
        <script src="{{ URL::asset('assets/libs/owl.carousel/owl.carousel.min.js')}}"></script>

        <!-- ICO landing init -->
        <script src="{{ URL::asset('assets/js/pages/ico-landing.init.js')}}"></script>

@endpush
