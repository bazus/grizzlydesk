@extends('layouts.page')

@section('body.attr')data-topbar="dark" data-layout="horizontal"@endsection

@push('page')
    <div id="layout-wrapper">

        <x-header/>

        {{-- <x-topnav/> --}}

        <div class="main-content">

            <div class="page-content" style="margin-top: 20px">
                @stack('page.content')
            </div>

            <x-footer/>
        </div>


    </div>
@endpush
