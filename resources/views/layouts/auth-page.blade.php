@extends('layouts.page')

@push('page')


    <div class="home-btn d-none d-sm-block">
        <a href="/" class="text-dark"><i class="fas fa-home h2"></i></a>
    </div>


    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">

                @stack('content')

            </div>
        </div>
    </div>

@endpush
