<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\{Company, IndustryCategory};
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use function redirect;
use function response;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('company.created', ['except' => ['create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Company::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        if(Auth::user()->company){
            return response(404);
        }

        return view('panel.company.create')->with([
            'industry_categories' => IndustryCategory::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function store(Request $request)
    {

        $this->validator($request->all())->validate();

        $this->insert($request->all());

        return redirect()->route('panel.dashboard');

    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     * @return Response
     */
    public function show(Company $company)
    {
        return $company;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return Response
     */
    public function edit(Company $company)
    {
        return $company;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return Response
     */
    public function destroy(Company $company)
    {
        //
    }

    /**
    * Get a validator for an incoming creation request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'size' => ['required', 'numeric', 'max:4', 'min:1'],
            'phone' => ['required', 'string', 'max:15'],
            'industry_slug' => ['required', 'string', 'exists:industries,slug'],
            'websiteurl'    => ['max:255']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function insert(array $data)
    {
        return Company::create([
            'owner_id' => Auth::id(),
            'industry_slug' => $data['industry_slug'],
            'name' => $data['name'],
            'size' => $data['size'],
            'phone' => $data['phone'],
            'websiteurl' => $data['websiteurl']
        ]);
    }
}
