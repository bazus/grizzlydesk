<?php

namespace App\Http\Controllers\Resources;

use App\Http\Controllers\Controller;
use App\Models\Leads;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

use Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use function response;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        return view('panel.leads.index')->with([
            'leads' => Leads::paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('panel.leads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $this->insert($request->all());

        return redirect()->route('panel.leads.index');
    }

    /**
     * Display the specified resource.
     *
     * @param $uuid
     * @return Application|Factory|Response|View
     */
    public function show($uuid)
    {
        return view('panel.leads.show')->with([
            'lead'  => Leads::find($uuid)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Leads $leads
     * @return Response
     */
    public function edit(Leads $leads)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Leads $leads
     * @return Response
     */
    public function update(Request $request, Leads $leads)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Leads $leads
     * @return Response
     */
    public function destroy(Leads $leads)
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'aditional' => ['nullable', 'string', 'max:1024'],
            'title' => ['required', 'string', 'max:255'],
            'client_uuid' => ['required', 'uuid']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Leads
     */
    protected function insert(array $data)
    {
        return Leads::create([
            'company_id' => Auth::user()->company->id,
            'client_uuid'=> $data['client_uuid'],
            'aditional' => $data['aditional'],
            'title' => $data['title']
        ]);
    }
}
