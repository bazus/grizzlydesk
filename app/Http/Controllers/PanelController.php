<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;

class PanelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        return view('panel.dashboard');
    }
    public function profile_settings()
    {
        return view('panel.profile.settings');
    }


}
