<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndustryCategory extends Model
{
    protected $fillable = [
        'name'
    ];
    public function industries()
    {
        return $this->hasMAny('App\Models\Industry', 'category_id');
    }
}
