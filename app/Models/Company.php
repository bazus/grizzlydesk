<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Company extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'owner_id', 'industry_slug', 'size', 'phone', 'websiteurl'
    ];

    public $timestamps = true;


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->slug = $query->slug ?? Str::slug($query->name, '-');
        });
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }
    public function clients()
    {
        return $this->hasMany('App\Models\Client', 'company_owner_id');
    }
}
