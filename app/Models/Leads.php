<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Leads extends Model
{
    protected $primaryKey = 'uuid';
    public $incrementing = false;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = $query->uuid ?? Str::orderedUuid();
        });
    }


    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_uuid', 'uuid');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

}
