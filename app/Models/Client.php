<?php /** @noinspection PhpUndefinedFieldInspection */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class Client extends Model
{

    protected $primaryKey = 'uuid';
    public $incrementing = false;

    protected $hidden = ['id'];

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->uuid = $query->uuid ?? Str::orderedUuid();
        });
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_owner_id');
    }
    public function name( $divider = " " ) {
        if($this->company_as_primary === true) return $this->companyname;
            return $this->firstname . $divider . $this->lastname;

    }

    public function leads(){
        return $this->hasMany('App\Models\Leads', 'client_uuid', 'uuid');
    }
}
