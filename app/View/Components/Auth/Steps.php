<?php

namespace App\View\Components\Auth;

use Illuminate\View\Component;

class Steps extends Component
{

    public $step;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($step)
    {
        $this->step = $step;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.auth.steps');
    }
}
