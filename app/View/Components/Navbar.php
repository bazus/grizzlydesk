<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class Navbar extends Component
{
    public $type;

    /**
     * Create a new component instance.
     *
     * @param string $type
     */
    public function __construct($type = 'sticky')
    {
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.navbar');
    }
}
