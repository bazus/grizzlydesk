<?php

namespace App\View\Components\Modals;

use Illuminate\View\Component;
use Faker\Factory;
use App\Models\Client;

class SelectUser extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.modals.select-user')->with([
            'faker' => Factory::create(),
            'clients' => Client::get()
        ]);
    }
}
