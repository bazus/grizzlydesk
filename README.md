##Installing

- ***`git clone`***
- ***`composer install`***
- ***`npm install`***
- ***`cp .env.example .env`***
- ***`php artisan key:generate`***
- ***`npm run prod`***
- ***`mcedit ( or any another editor you use ) .env`***
