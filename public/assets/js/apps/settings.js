function preloadImages(array) {
  if (!preloadImages.list) {
    preloadImages.list = [];
  }

  var list = preloadImages.list;

  for (var i = 0; i < array.length; i++) {
    var img = new Image();

    img.onload = function () {
      var index = list.indexOf(this);

      if (index !== -1) {
        list.splice(index, 1);
      }
    };

    list.push(img);
    img.src = array[i];
  }
}

function formatState(data, container) {
  if (!data.id) return data.text;
  return $('<span><img src="' + data.flag + '" class="img-flag" /> ' + data.text + '</span>');
}

function getCountries() {
  var images = [];
  $.getJSON('/resources/counties.json', function (data) {
    var countries = data.countries.map(function (e) {
      return {
        id: e.code,
        text: e.name,
        flag: e.flag
      };
    });
    $('.country-select').select2({
      data: countries,
      templateResult: formatState
    });
  });
}

$(document).ready(function () {
  getCountries();
});
