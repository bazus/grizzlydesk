$(document).ready(function () {
  var tableRequests = $('.table-requests').DataTable({
    sDom: 'lrtip',
    lengthChange: false,
    pageLength: 5,
    "language": {
      "paginate": {
        "previous": '<i class="mdi mdi-chevron-left"></i>',
        "next": '<i class="mdi mdi-chevron-right"></i>'
      }
    },
    "drawCallback": function drawCallback() {
      $(".table-requests-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    }
  });
  var tableQuotes = $('.table-quotes').DataTable({
    sDom: 'lrtip',
    lengthChange: false,
    pageLength: 5,
    "language": {
      "paginate": {
        "previous": '<i class="mdi mdi-chevron-left"></i>',
        "next": '<i class="mdi mdi-chevron-right"></i>'
      }
    },
    "drawCallback": function drawCallback() {
      $(".table-qoutes-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    }
  });
  var tableJobs = $('.table-jobs').DataTable({
    sDom: 'lrtip',
    lengthChange: false,
    pageLength: 5,
    "language": {
      "paginate": {
        "previous": '<i class="mdi mdi-chevron-left"></i>',
        "next": '<i class="mdi mdi-chevron-right"></i>'
      }
    },
    "drawCallback": function drawCallback() {
      $(".table-jobs-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    }
  });
  var tableInvoices = $('.table-invoices').DataTable({
    sDom: 'lrtip',
    lengthChange: false,
    pageLength: 5,
    "language": {
      "paginate": {
        "previous": '<i class="mdi mdi-chevron-left"></i>',
        "next": '<i class="mdi mdi-chevron-right"></i>'
      }
    },
    "drawCallback": function drawCallback() {
      $(".table-invoices-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    }
  });
  $(".table-requests-paginaion-container").append($("#DataTables_Table_0_paginate"));
  $(".table-requests-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
  $(".table-qoutes-paginaion-container").append($("#DataTables_Table_1_paginate"));
  $(".table-qoutes-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
  $(".table-jobs-paginaion-container").append($("#DataTables_Table_2_paginate"));
  $(".table-jobs-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
  $(".table-invoices-paginaion-container").append($("#DataTables_Table_3_paginate"));
  $(".table-invoices-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
});
