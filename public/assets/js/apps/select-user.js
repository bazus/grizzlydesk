function setUser(userUUID) {
  $.ajax({
    url: '/api/clients/' + userUUID,
    success: function success(user) {
      $('.choose-user').modal('hide');
      $('.user-name').html(user.firstname + ' ' + user.lastname);
      $('input[name="client_uuid"]').val(user.uuid);
    }
  });
}

$(document).ready(function () {
  $('tr[data-user]').on('click', function () {
    setUser($(this).attr('data-user'));
  });
  var table = $('#datatable-buttons').DataTable({
    sDom: 'lrtip',
    lengthChange: false,
    pageLength: 5,
    "language": {
      "paginate": {
        "previous": '<i class="mdi mdi-chevron-left"></i>',
        "next": '<i class="mdi mdi-chevron-right"></i>'
      }
    },
    "drawCallback": function drawCallback() {
      $(".table-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
    }
  });
  $(".table-paginaion-container").append($(".dataTables_paginate"));
  $(".table-paginaion-container").find('.pagination').addClass('pagination-rounded justify-content-center mt-4');
  $('#form-input-search').keyup(function () {
    table.search($(this).val()).draw();
  });
});
