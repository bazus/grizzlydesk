<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('guest')->namespace('Auth')->prefix('auth')->name('auth.')->group(function () {
    Route::name('page.')->group(function () {
        Route::get('login', 'LoginController@index')->name('login');
        Route::get('register', 'RegisterController@index')->name('register');
    });

    Route::name('request.')->group(function () {
        Route::post('login', 'LoginController@login')->name('login');
        Route::post('register', 'RegisterController@register')->name('register');
    });
});
Route::get('logout', function () {
    Auth::logout();
    return redirect(route('auth.page.login'));
})->name('logout');

//Panel
Route::middleware('auth')->name('panel.')->prefix('panel')->group(function () {

    Route::resource('company', 'Resources\CompanyController');

    Route::middleware('company.created')->group(function () {

        Route::prefix('api')->name('API')->group(function (){
            Route::resources([
                'clients' => 'ClientController'
            ]);
        });

        Route::resources([
            'clients' => 'Resources\ClientController',
            'leads' => 'Resources\LeadsController',
        ]);

        Route::get('dashboard', 'PanelController@dashboard')->name('dashboard');

        Route::prefix('profile')->name('profile.')->group(function (){
            Route::get('settings', 'PanelController@profile_settings')->name('settings');
        });

        Route::get('dashboard', 'PanelController@dashboard')->name('dashboard');

    });
});
//End panel

//ClientZone

Route::prefix('clientzone')->name('clientzone')->namespace('ClientZone')->group(function (){

});


Route::view('/', 'website.landing')->name('landing');
Route::view('/pricing', 'website.pricing')->name('pricing');
