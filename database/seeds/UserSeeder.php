<?php

use Illuminate\Database\Seeder;
use App\{User, Models\Company, Models\Industry, Models\Client, Models\Leads};
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 5; $i++){
            $user = User::create([
                'lastname' => $faker->lastName,
                'firstname' => $faker->firstName,
                'email' => $faker->unique()->email,
                'password' => Hash::make('cderfv123'),
            ]);
            $company = Company::create([
                'name' => $faker->unique()->company,
                'owner_id' => $user->id,
                'industry_slug' => Industry::inRandomOrder()->first()->slug,
                'size'  => $faker->numberBetween(1, 4),
                'phone' => $faker->e164PhoneNumber,
                'websiteurl'   => $faker->url
            ]);
            for($y = 0; $y < rand(5, 10); $y++){
                $client = Client::create([
                    'uuid' => $faker->uuid,
                    'company_owner_id' => $company->id,
                    'firstname' => $faker->firstName,
                    'lastname'  => $faker->lastName,
                    'companyname' => $faker->company,
                    'aditional' => $faker->paragraph,
                    'company_as_primary' => $faker->numberBetween(0, 1),
                    'email' => $faker->email,
                    'phone' => $faker->e164PhoneNumber
                ]);
                for($z = 0; $z < rand(4, 7); $z++){
                    $lead = Leads::create([
                        'uuid'  => $faker->uuid,
                        'title' => $faker->sentence,
                        'company_id' => $company->id,
                        'client_uuid' => $client->uuid,
                        'manual_created' => $faker->numberBetween(0, 1),
                        'aditional' => $faker->paragraph
                    ]);
                }
            }
        }

    }
}
