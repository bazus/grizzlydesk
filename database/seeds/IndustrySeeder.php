<?php

use Illuminate\Database\Seeder;
use App\Models\Industry;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Industry::insert([
            ['name' => 'Carpet Cleaning', 'slug' => 'Carpet_Cleaning', 'category_id' => 1],
            ['name' => 'Commercial Cleaning', 'slug' => 'Commercial_Cleaning', 'category_id' => 1],
            ['name' => 'Pressure Washing Service', 'slug' => 'Pressure_Washing_Service', 'category_id' => 1],
            ['name' => 'Residential Cleaning', 'slug' => 'Residential_Cleaning', 'category_id' => 1],
            ['name' => 'Window Washing', 'slug' => 'Window_Washing', 'category_id' => 1],


            ['name' => 'Arborist / Tree Care', 'slug' => 'Arborist_Tree_Care', 'category_id' => 2],
            ['name' => 'Landscaping Contractor', 'slug' => 'Landscaping_Contractor', 'category_id' => 2],
            ['name' => 'Lawn Care & Lawn Maintenance', 'slug' => 'Lawn_Care_Lawn_Maintenance', 'category_id' => 2],


            ['name' => 'Computers & IT', 'slug' => 'Computers_IT', 'category_id' => 3],
            ['name' => 'Home Theater', 'slug' => 'Home_Theater', 'category_id' => 3],
            ['name' => 'Lawn CarSecurity and Alarm', 'slug' => 'Lawn_CarSecurity_and_Alarm', 'category_id' => 3],


            ['name' => 'Construction & Contracting', 'slug' => 'Construction_Contracting', 'category_id' => 4],
            ['name' => 'Electrical Contractor', 'slug' => 'Electrical_Contractor', 'category_id' => 4],
            ['name' => 'HVAC', 'slug' => 'HVAC', 'category_id' => 4],
            ['name' => 'Locksmith', 'slug' => 'Locksmith', 'category_id' => 4],
            ['name' => 'Mechanical Service', 'slug' => 'Mechanical_Service', 'category_id' => 4],
            ['name' => 'Plumbing', 'slug' => 'Plumbing', 'category_id' => 4],


            ['name' => 'Appliance Repair', 'slug' => 'Appliance_Repair', 'category_id' => 5],
            ['name' => 'Flooring Service', 'slug' => 'Flooring_Service', 'category_id' => 5],
            ['name' => 'Handyman', 'slug' => 'Handyman', 'category_id' => 5],
            ['name' => 'Junk Removal', 'slug' => 'Junk_Removal', 'category_id' => 5],
            ['name' => 'Painting', 'slug' => 'Painting', 'category_id' => 5],
            ['name' => 'Pest Control', 'slug' => 'Pest_Control', 'category_id' => 5],
            ['name' => 'Pool and Spa Service', 'slug' => 'Pool_and_Spa_Service', 'category_id' => 5],
            ['name' => 'Renovations', 'slug' => 'Renovations', 'category_id' => 5],
            ['name' => 'Roofing Service', 'slug' => 'Roofing_Service', 'category_id' => 5],
            ['name' => 'Snow Removal', 'slug' => 'Snow_Removal', 'category_id' => 5],

            ['name' => 'Other', 'slug' => 'Other', 'category_id' => 5],

        ]);
    }
}
