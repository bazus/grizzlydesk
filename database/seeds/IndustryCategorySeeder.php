<?php

use Illuminate\Database\Seeder;
use App\Models\IndustryCategory;

class IndustryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IndustryCategory::insert([
            ['name' => 'Clearing'],
            ['name' => 'Green Industry'],
            ['name' => 'Hi Tech'],
            ['name' => 'Trade'],
            ['name' => 'Other']
        ]);
    }
}
